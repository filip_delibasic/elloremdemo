import React, {Component} from 'react';
import {WebView} from 'react-native-webview';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob';
import {Platform} from 'react-native';

class App extends Component {
  webref = null;
  render() {
    if (Platform.OS === 'android') {
      return (
        <WebView
          ref={r => (this.webref = r)}
          originWhitelist={['*']}
          source={{
            uri: 'https://ellorem.xyz/welcome',
          }}
          javaScriptEnabled
          onMessage={event => {
            const options = {
              title: 'Select Avatar',
              storageOptions: {
                skipBackup: true,
                path: 'images',
              },
            };
            if (event.nativeEvent.data === 'file_upload') {
              ImagePicker.showImagePicker(options, response => {
                if (response.didCancel) {
                  console.log('User cancelled image picker');
                } else if (response.error) {
                  console.log('ImagePicker Error: ', response.error);
                } else if (response.customButton) {
                  console.log(
                    'User tapped custom button: ',
                    response.customButton,
                  );
                } else {
                  console.log('Sending img');
                  /*
                  this.webref.postMessage(JSON.stringify(response));
                  */

                  const file = {
                    name: 'file',
                    data: RNFetchBlob.wrap(response.uri),
                    type: response.type,
                    filename: response.fileName,
                  };
                  RNFetchBlob.fetch(
                    'POST',
                    'https://api.ellorem.xyz/public/file-upload',
                    {'Content-Type': 'multipart/form-data'},
                    [file],
                  )
                    .then(res => {
                      console.log(res.data);

                      this.webref.postMessage(res.data);
                    })
                    .catch(error => {
                      this.webref.postMessage(JSON.stringify(error));
                    });
                }
              });
            } else {
              console.log(event.nativeEvent.data);
            }
          }}
        />
      );
    } else {
      <WebView
        source={{
          uri: 'https://ellorem.xyz/welcome',
        }}
      />;
    }
  }
}

export default App;
